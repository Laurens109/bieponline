@extends('layouts.app') 

@section('title')
	({{$role->id}}) {{$role->name}} <a class="btn btn-default" href="{{action('RoleController@edit', $role->id)}}"><i class="fa fa-pencil"></i></a>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-2">
		{!! Form::open(['route' => ['role.destroy', $role->id], 'method'=>'DELETE']) !!}
		{!! Form::submit('Delete', array('class'=>'btn btn-danger')) !!}
		{!! Form::close() !!}
	</div>
	<br/>
</div>
@if (count($role->users) > 0)
	<table class="table table-striped table-hover">
		<thead>
		<th class="col-sm-1">Id</th>
		<th class="col-sm-4">Naam</th>
		<th class="col-sm-2"><i class="fa fa-at"></i></th>
		</thead>
		<tbody>
		{{-- table which shows all users associated with the role --}}
		@foreach ($role->users as $user)
			<tr class="row-link" style="cursor: pointer;"
				data-href="{{action('UserController@show', ['id' => $user->id]) }}">
				<td class="table-text">{{ $user->id }}</td>
				<td class="table-text">{{ $user->name }}</td>
				<td class="table-text"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>
@endif
@endsection

@section('scripts')
	<script>
		jQuery(document).ready(function($) {
			$(".row-link").click(function() {
				window.document.location = $(this).data("href");
			});
			$('#cohort-tabs a:first').tab('show') // Select first tab
		});
	</script>
@endsection
